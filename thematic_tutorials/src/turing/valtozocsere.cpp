#include <stdio.h>

int main (void){
	int a = 3, b = 2;

  	printf ("a=%d, b=%d\n", a, b);
	// kivonás, összeadás segítségével:
  	a = a - b;
  	b = a + b;
  	a = b - a;
	
	// csere változó segítségével:
	int c = a;
	a = b;
	b = c;

  	printf ("a=%d, b=%d\n", a, b);

  	return 0;
}

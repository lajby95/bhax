#include <stdio.h>
#include <unistd.h> 
#include <sys/types.h>

int main (void){

	pid_t pids[4];
	int i;
	int n = 4;

	/* gyerek processek indítása */
	for (i = 0; i < n; ++i) {
	  if ((pids[i] = fork()) < 0) {
	    perror("fork");
	    return 0;
	  } else if (pids[i] == 0) {
	    for (;;){	// a végtelen ciklus futtatása minden újabb gyerek processen
			printf("fut pids[%d]\n", i);
		}
	  }
	}

  	return 0;
}
